%!TEX root=../cst.tex

%===============================================================================
\section{Travail Scientifique}
\label{sec:travaux}

%-------------------------------------------------------------------------------
\subsection{Premiers Travaux}

\paragraph{Benchmark} La mise en place d'une démarche d'optimisation a nécessité
l'élaboration d'un modèle des déplacements urbains. Nous avons opté pour le
développement d'un outil de benchmarking qui, à partir du réseau routier de la
ville étudiée, génère une multitude de scénarios de mobilité. Un scénario
consiste en une définition d'emplois du temps pour les voyageurs se déplaçant au
sein de la ville. Par exemple, il est possible de définir l'emploi du temps de
10000 voyageurs, et pour chaque voyageur, de spécifier s'il se déplace jusqu'à
son lieu de travail ou un lieu de loisir, la durée de son activité, à partir de
quelle heure, etc. Cet outil de benchmarking est conçu de la façon la plus
générique possible. En effet, les flux urbains d'une ville évoluent dans le
temps (renouvellement de la voirie, construction de nouveaux pôles attractifs).
De plus, certaines données, relatives au recensement de la population ou de sa
répartition dans la ville, sont parfois manquantes ou inaccessibles. Le
développement de cet outil de benchmarking nous permettra d'étudier une série de
scénarios de mobilité plausibles, avec plus ou moins de précision, selon les
données qui seront mises à notre dispotion. Il permettra également de comparer
les performances de divers algorithmes d'optimisation pour des problèmes de
mobilité urbaine. Pour ces premiers travaux, nous avons donc développé le
benchmark \textsc{sialac} : \emph{Scenarios Investigations of Agents Locations
for Algorithm Conception}. Il définit 72 scénarios de mobilité dans la ville de
Calais. Le parcours d'un voyageur consiste en un aller-retour entre son domicile
et son lieu de travail. Un exemple de scénario est représenté à la Figure
\ref{fig:sialac}.

\begin{figure}
  \includegraphics[width=\columnwidth]{img/sialac}
  \caption{Répartition des voyageurs dans 4 \emph{quartiers}. Chaque voyageur
    effectue un trajet jusqu'à son lieu de travail (qui n'est pas représenté sur
    cette figure). }
  \label{fig:sialac}
\end{figure}

\paragraph{Modélisation des feux de signalisation} Nous avons exploité les
instances de \textsc{sialac} pour l'optimisation du réglage des feux de
signalisation dans la ville de Calais. Chaque carrefour à feu est modélisé par
plusieurs paramètres : le temps de vert sur les différents axes, le temps de
cycle (qui correspond à la somme des temps de vert et d'orange pour chaque axe),
et un temps de \emph{décalage} utile à la synchronisation des feux. Une
représentation de ce réglage est proposé à la Figure \ref{fig:feux}. Chaque
carrefour à feu est donc paramétré par un réglage $S$, et le réglage de la
totalité des feux de la ville peut être exprimé par un vecteur
$x=(S_1,\dots,S_n)$, avec $n$ le nombre de carrefours à feux. Cette modélisation
est basée sur les travaux de l'université de Shinshu, Nagano \cite{Armas2016,
Armas2016_2}.

\begin{figure}
  \includegraphics[width=\columnwidth]{img/feux}
  \caption{Représentation du réglage d'un carrefour à feux. $C$: temps de cycle,
    $\Phi$: temps de vert, $\Theta$: temps de décalage. }
  \label{fig:feux}
\end{figure}

\paragraph{Simulateur} Le simulateur MATSim \cite{MATSim} (Multi-Agent Transport
Simulation) a été employé pour simuler l'ensemble des scénarios de mobilité
générés par \textsc{sialac}. La simulation d'un scénario, selon un paramétrage
spécifique des feux de signalisation, dure approximativement 1 minute. L'espace
de recherche grandit rapidement avec le nombre de feux à régler. Aussi, le
développement d'algorithmes d'optimisation efficaces est envisagé, afin de
trouver un réglage optimal des feux tout en consommant le moins de temps de
simulation possible.

%-------------------------------------------------------------------------------
\subsection{Algorithmes d'Optimisation}
\label{subsec:algos}

\paragraph{Problèmes d'optimisation et recherche locale} Un problème
d'optimisation est un couple ($\mathcal{X}, f)$, avec $\mathcal{X}$ l'espace de
recherche et $f:\mathcal{X}\rightarrow\mathbb{R}$ le critère de qualité à
optimiser \cite{Stadler:2002}. Ici, le critère de qualité sera le temps de parcours
moyen des voyageurs dans la ville, que nous chercherons à minimiser. Le problème
d'optimisation du réglage des feux étant un problème \emph{boîte noire} (la
définition analytique du problème n'est pas connue), combinatoire, et coûteux
(en termes de temps de simulation), nous utilisons, dans le cadre de ces
premiers travaux, des algorithmes de recherche locale à solution unique (à la
différence des algorithmes à population de solutions). Ces méthodes
d'optimisation sont basées sur la relation de voisinage entre les solutions.

\paragraph{Paysages de fitness et variables sensibles} Grâce à la variété des
scénarios de mobilité générés par le benchmark, nous pouvons analyser les
paysages de fitness engendrés par notre problème d'optimisation. Un paysage
de fitness fournit une représentation métaphorique de la géométrie de l'espace de
recherche étudié (vallées, plateaux, pics). Cette analyse permet de définir un
ensemble de métriques (coefficients d'autocorrelation \cite{Weinberger:1990},
entropie maximale \cite{Vassilev:2003}) afin de quantifier et de comparer la
difficulté de la recherche des solutions optimales. Cette analyse des paysages
de fitness engendrés par les 72 instances de \textsc{sialac} nous permet
l'extraction des variables sensibles du problème d'optimisation. En d'autre
termes, nous sommes capables de repérer les carrefours à feux de la ville qui
impacteront le plus le temps de parcours des voyageurs. Une étude plus
approfondie est détaillée dans \cite{Lepretre2018}.

\paragraph{Algorithme adaptatif} L'extraction des variables sensibles nous
permet de concevoir un algorithme d'optimisation adaptatif, qui va concentrer la
recherche des réglages optimaux sur les carrefours à feux critiques, sans pour
autant négliger le réglage de ceux qui le sont moins. Nous avons conçu cet
algorithme à l'aide d'une technique d'apprentissage artificiel (\emph{Upper
Confidence Bound} \cite{Auer_2002_ML}). Il s'avère que cet algorithme adaptatif
fournit des réglages optimaux avec un nombre d'itérations moindre que celui
nécessaire à un algorithme de recherche locale classique par descente de
gradient, économisant ainsi un précieux temps de simulation. Un exemple de
minimisation du temps de parcours des agents pour une instance spécifique de
\textsc{sialac} est représenté à la Figure \ref{fig:perfs}.

\begin{figure}
  \includegraphics[width=\columnwidth]{img/perfs}
  \caption{Minimisation du temps de parcours moyen des voyageurs pour une
    instance de \textsc{sialac}, par une méthode classique de descente de
    gradient et l'algorithme adaptatif proposé. A la fin de l'optimisation, les
    algorithmes ont convergé vers un réglage des feux de signalisation optimal.}
  \label{fig:perfs}
\end{figure}

%-------------------------------------------------------------------------------
\subsection{Conclusion}

\paragraph{Discussion} L'analyse des paysages de fitness engendrés par les
nombreuses instances de \textsc{sialac} nous permet de détecter les variables
(carrefours à feux) sensibles. Nous nous basons sur cette analyse pour concevoir
un algorithme adaptatif basé sur une méthode d'apprentissage artificiel, afin de
concentrer l'optimisation sur ces-dites variables. Afin de \emph{valider} le
benchmark et la méthodologie présentés, nous avons réitéré nos protocoles
expérimentaux (génération de scénarios de mobilité, analyse des paysages
de fitness) sur la ville de Quito, Equateur, et avons obtenu des résultats
équivalents. Le benchmark \textsc{sialac} correspond aux attentes que nous
espérions : il fournit des scénarios de mobilité plausibles au sein d'une ville,
qui peuvent être simulés afin de fournir une quantité d'informations pour toute
sorte de travaux connexes (par exemple, dans notre cas, l'optimisation des
réglages des feux de signalisation de la ville de Calais ou Quito).

\paragraph{Perspectives} Nous proposons de réutiliser les outils de benchmarking
présentés pour résoudre un tout autre problème d'optimisation lié à la mobilité
dans Calais, à savoir déterminer le positionnement optimal des arrêts de bus
pour la nouvelle ligne en site propre envisagée par la ville. Par la même
occasion, du fait que cette problématique puisse s'apparenter à un problème
pseudo-booléen, nous nous proposons d'utiliser des méthodes basées sur la
décomposition en fonctions de Walsh \cite{Verel:2018} afin de concevoir un méta-modèle du
simulateur.
